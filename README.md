# Animate

[https://bit.ly/kurz-animate](https://bit.ly/kurz-animate)

## Nevidíte data z vašeho kurzu?
- Najděte rozbalovací nabídku kousek nahoře vlevo nad seznamem souborů, ve které vidíte slovo „master“ (vedle ní je text „ kurz-animate“).
- Klikněte na ni.
- V sekci Tags klikněte na datum Vašeho kurzu.
